@file:Suppress("EXPERIMENTAL_API_USAGE")

import curl.*
import kotlinx.cinterop.*
import platform.posix.EXIT_FAILURE
import platform.posix.fprintf
import platform.posix.stderr
import kotlin.system.exitProcess

fun main(args: Array<String>) {
    checkArgs(args)
    val url = args.first()
    val curlHandle = setupCurl(url)
    doHttpGet(curlHandle, url)
    curl_easy_cleanup(curlHandle)
}

private fun doHttpGet(curlHandle: CPointer<out CPointed>?, url: String) {
    val result = curl_easy_perform(curlHandle)
    if (result != CURLE_OK) {
        val errorMsg = curl_easy_strerror(result)?.toKString() ?: "Unknown error"
        fprintf(stderr, "Cannot do HTTP call to $url (Code $result - $errorMsg).\n")
        exitProcess(EXIT_FAILURE)
    }
}

private fun setupCurl(url: String): COpaquePointer? {
    val result = curl_easy_init()
    if (result == null) {
        fprintf(stderr, "Cannot initialise Curl.\n")
        exitProcess(EXIT_FAILURE)
    }
    curl_easy_setopt(result, CURLOPT_URL, url)
    curl_easy_setopt(result, CURLOPT_WRITEFUNCTION, staticCFunction(::readHttpResponseBody))
    return result
}

private fun readHttpResponseBody(
    buffer: CPointer<ByteVar>,
    @Suppress("UNUSED_PARAMETER") itemSize: ULong,
    @Suppress("UNUSED_PARAMETER") totalItems: ULong,
    @Suppress("UNUSED_PARAMETER") userData: COpaquePointer
): ULong {
    println("HTTP Response Body:")
    buffer.toKString().trim().split("\n").forEachIndexed { pos, item ->
        println("${pos + 1}: $item")
    }
    return itemSize * totalItems
}

private fun checkArgs(args: Array<String>) {
    if (args.size != 1) {
        fprintf(stderr, "A single program argument must be provided for the URL.\n")
        exitProcess(EXIT_FAILURE)
    }
}
