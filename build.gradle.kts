group = "org.example"
version = "0.1-SNAPSHOT"

plugins {
    kotlin("multiplatform") version "1.4.10"
}

kotlin {
    linuxX64 {
        compilations.getByName("main") {
            cinterops.create("libcurl") {
                includeDirs("/usr/include/x86_64-linux-gnu/curl")
            }
        }
        binaries {
            executable {
                entryPoint = "main"
            }
        }
    }
}
